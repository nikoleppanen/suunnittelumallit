/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_factory;

/**
 *
 * @author Niko
 */
public class AdFactory implements IBrandFactory {

    @Override
    public IShoes createShoes() {
        return new AdShoes();
    }

    @Override
    public ITshirt createTshirt() {
        return new AdTshirt();
    }

    @Override
    public IJeans createJeans() {
        return new AdJeans();
    }

    @Override
    public ICap createCap() {
        return new AdCap();
    }
    
}
