/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_factory;

/**
 *
 * @author Niko
 */
public class BossFactory implements IBrandFactory {

    @Override
    public IShoes createShoes() {
        return new BossShoes();
    }

    @Override
    public ITshirt createTshirt() {
        return new BossTshirt();
    }

    @Override
    public IJeans createJeans() {
        return new BossJeans();
    }

    @Override
    public ICap createCap() {
        return new BossCap();
    }
    
}
