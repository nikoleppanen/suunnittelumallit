/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_factory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Class c = null;
        IBrandFactory factory = null;
        
        //Properties
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream("factory.properties"));
        }catch (IOException e){e.printStackTrace();}
        
        try {
            c = Class.forName(properties.getProperty("factory"));
            factory = (IBrandFactory)c.newInstance();
        } catch (Exception ex) {ex.printStackTrace();}
        
        ICap cap = factory.createCap();
        IJeans jeans = factory.createJeans();
        IShoes shoes = factory.createShoes();
        ITshirt tshirt = factory.createTshirt();
        
        System.out.println("Today I have been mostly wearing "+cap+" "+ jeans +" "+ shoes +" "+ tshirt);
    }
    
    
}
