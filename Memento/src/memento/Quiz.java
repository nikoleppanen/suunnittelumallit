/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Niko
 */
public class Quiz {
    
    private Randomizer randomizer;
    
    public Quiz(){
        
        randomizer = new Randomizer();
        
    }
    
    public Memento joinGame(){
        
        return new Memento(randomizer.getRandomNumber());
        
    }
    
    public static boolean guess(Memento memento, int guess){
        
        return memento.getNumber() == guess;
        
    }
    
}
