/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Random;

/**
 *
 * @author Niko
 */
public class Randomizer {
    
    private Random random;
    
    public Randomizer(){
        random = new Random();
    }
    
    public int getRandomNumber(){
        return random.nextInt(10);
    }
}
