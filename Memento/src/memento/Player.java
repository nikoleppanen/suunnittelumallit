/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Scanner;

/**
 *
 * @author Niko
 */
public class Player extends Thread {
    
    private Memento memento;
    private String name;
    
    public Player(String name){
        
        this.name = name;
    }
    
    public void setMemento(Memento memento){
        
        this.memento = memento;
    }
    
    @Override
    public void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Take a guess!");
        while(!Quiz.guess(memento, scanner.nextInt())){
            System.out.println("Take a guess!");
        }
        System.out.println("You wins!");

    }
    
}
