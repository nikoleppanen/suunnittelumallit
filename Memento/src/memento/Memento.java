/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Niko
 */
public class Memento {
    
    private int number;
    
    public Memento(int number){
        
        this.number = number;
        
    }
    
    public int getNumber(){
        
        return this.number;
        
    }
    
}
