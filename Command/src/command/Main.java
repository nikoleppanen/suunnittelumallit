/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Screen screen = new Screen();
        
        Command raise = new RaiseCommand(screen);
        Command lower = new LowerCommand(screen);
        
        Button r_button = new Button(raise);
        Button l_button = new Button(lower);
        
        l_button.push();
        r_button.push();
        
    }
    
}
