/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author Niko
 */
public class Button {

    Command cmd;
    
    public Button(Command cmd){
        this.cmd = cmd;
    }

    public void push() {
    
        cmd.execute();
    }
}

