/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Niko
 */
public class Charmaleon extends PokemonState {

    @Override
    void attack() {
        System.out.println(this.getState()+" uses Fire Fang!");
    }

    @Override
    void powerUp() {
        System.out.println(this.getState()+" is powering up!");
    }

    @Override
    void evolve(Pokemon poke, final PokemonState state) {
        poke.state = state;
    }

    @Override
    String getState() {
        return "Charmaleon";
    }
    
}
