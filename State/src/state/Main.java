/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pokemon poke = new Pokemon();
        poke.attack();
        poke.powerUp();
        
        poke.evolve(poke, new Charmaleon());
        poke.attack();
        poke.powerUp();
        
        poke.evolve(poke, new Charizard());
        poke.attack();
        poke.powerUp();
    }
    
}
