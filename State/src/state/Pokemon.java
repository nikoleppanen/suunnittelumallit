/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Niko
 */
public class Pokemon extends PokemonState {
    
    public Pokemon(){
        this.state = new Charmander();
    }

    @Override
    void attack() {
        this.state.attack();
    }

    @Override
    void powerUp() {
        this.state.powerUp();
    }

    @Override
    void evolve(Pokemon poke, PokemonState state) {
        this.state.evolve(poke, state);
    }

    @Override
    String getState() {
        return this.state.getState();
    }
}
