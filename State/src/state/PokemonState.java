/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Niko
 */
abstract class PokemonState {
    
    PokemonState state;
    
    abstract String getState();
    
    abstract void attack();
    
    abstract void powerUp();
    
    abstract void evolve(Pokemon poke, PokemonState state);
}
