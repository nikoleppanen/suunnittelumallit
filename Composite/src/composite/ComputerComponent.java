/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Niko
 */
public interface ComputerComponent {
    
   abstract int getPrice();
   
   abstract void setPrice(int new_price);
   
}
