/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Niko
 */
public class MotherboardComposite implements ComputerComponent {
    
    private int price = 100;

    private List <ComputerComponent> childComponents = new ArrayList();
    
    public void addComponent(ComputerComponent component){
        
        childComponents.add(component);
    }
    
    public void removeComponent(ComputerComponent component){
        
        childComponents.remove(component);
    }
    
    @Override
    public String toString(){
        String str = "Motherboard";
        
        if(!childComponents.isEmpty()){
        for(ComputerComponent component : childComponents){
            str = str + " + " +component.toString();
        }}
        return str;
    }

    @Override
    public int getPrice() {
        if(!childComponents.isEmpty()){
        for(ComputerComponent component : childComponents){
            price = price + component.getPrice();
        }}
        
        return price;
    }


    @Override
    public void setPrice(int new_price) {
        price = new_price;
    }
    
}
