/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CaseComposite case1 = new CaseComposite();
        MotherboardComposite mobo = new MotherboardComposite();
        mobo.addComponent(new NetworkAdaptor());
        mobo.addComponent(new Processor());
        mobo.addComponent(new RAM());
        case1.addComponent(mobo);
        case1.addComponent(new GraphicsCard());
        System.out.println(case1 + ": " + case1.getPrice());
    }
    
}
