/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Niko
 */
public class NetworkAdaptor implements ComputerComponent {
    
    int price = 25;

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int new_price) {
        price = new_price;
    }
    @Override
    public String toString(){
        return "Network Adaptor";
    }
    
}
