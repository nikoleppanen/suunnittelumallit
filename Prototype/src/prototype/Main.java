/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.CloneNotSupportedException
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Watch watch = new Watch();
        
        watch.getHands().setSec(1);
        
        Watch clone_watch = (Watch) watch.clone();
        
        watch.getHands().setSec(2);
        
        clone_watch.getHands().setMin(1);
        
        System.out.println(watch);
        System.out.println(clone_watch);
    }
    
}
