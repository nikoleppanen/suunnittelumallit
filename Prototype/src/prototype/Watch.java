/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Niko
 */
public class Watch implements Cloneable {
    
    private Hands hands;

    public Hands getHands() {
        return hands;
    }

    public void setHands(Hands hands) {
        this.hands = hands;
    }
    
    public Watch(){
        
        hands = new Hands();
        
    }
    
    @Override
    public String toString(){
        return hands.getHr()+":"+hands.getMin()+":"+hands.getSec();
    }
    
    //SYVÄKOPIO
    @Override
    public Object clone() throws CloneNotSupportedException{
        
        Watch watch = null;
        
        try{
            watch = (Watch)super.clone();
            watch.hands = (Hands)hands.clone();
            
        }catch(CloneNotSupportedException e){
            
            return null;
        }
        
        return watch;
    }
}
