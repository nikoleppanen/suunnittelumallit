/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Niko
 */
public class Hands implements Cloneable {
    
    private int sec, min, hr;

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getHr() {
        return hr;
    }

    public void setHr(int hr) {
        this.hr = hr;
    }
    
    public Hands(int sec,int min,int hr){
        this.sec = sec;
        this.min = min;
        this.hr = hr;
    }
    
    public Hands(){
        
    }
    
    //MATALAKOPIO
    @Override
    public Object clone() throws CloneNotSupportedException{
        try{
            return super.clone();
        }catch(CloneNotSupportedException e){
            return null;
        }
    }
}
