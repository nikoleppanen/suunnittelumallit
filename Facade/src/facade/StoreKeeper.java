/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Niko
 */


public class StoreKeeper {
    
    public StoreKeeper(){ }
    
    /**
    * The raw materials are asked for and
    * are returned
    *
    * @return raw materials
    */

    public RawMaterialGoods getRawMaterialGoods() {

        RawMaterialStore store = new RawMaterialStore();

        RawMaterialGoods rawMaterialGoods = (RawMaterialGoods)store.getGoods();

        return rawMaterialGoods;

    }


    /**
    * The packaging materials are asked for and
    * are returned
    *
    * @return packaging materials
    */

    public PackingMaterialGoods getPackingMaterialGoods() {

        PackingMaterialStore store = new PackingMaterialStore();

        PackingMaterialGoods packingMaterialGoods = (PackingMaterialGoods)store.getGoods();

        return packingMaterialGoods;
}


    /**
    * The finished goods are asked for and
    * are returned
    *
    * @return finished goods
    */

    public FinishedGoods getFinishedGoods() {

        FinishedGoodsStore store = new FinishedGoodsStore();

        FinishedGoods finishedGoods = (FinishedGoods)store.getGoods();

        return finishedGoods;

    }
    
    public Goods getGoods(String goodsType) {
        
        if (goodsType.equals("Packaging")) {
            PackingMaterialStore store = new PackingMaterialStore();
            PackingMaterialGoods packingMaterialGoods = (PackingMaterialGoods)store.getGoods();
            return packingMaterialGoods;
        }
        else if (goodsType.equals("Finished")) {
            FinishedGoodsStore store = new FinishedGoodsStore();
            FinishedGoods finishedGoods = (FinishedGoods)store.getGoods();
        return finishedGoods;
        }
        else {
            RawMaterialStore store = new RawMaterialStore();
            RawMaterialGoods rawMaterialGoods = (RawMaterialGoods)store.getGoods();
        return rawMaterialGoods;
        }

    }// End of class
}// End of class
