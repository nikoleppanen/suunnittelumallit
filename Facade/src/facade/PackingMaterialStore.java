/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Niko
 */
public class PackingMaterialStore implements Store {

    @Override
    public Goods getGoods() {
        
        PackingMaterialGoods packingMaterialGoods = new PackingMaterialGoods();
        return packingMaterialGoods;
    }
    
}
