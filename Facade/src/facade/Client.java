/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Niko
 */


public class Client {
     /*
     * to get raw materials
     * @param args
     */
    

    public static void main(String[] args) {

        StoreKeeper keeper = new StoreKeeper();
        
        //GET RAWMATERIALS
        RawMaterialGoods rawMaterialGoods = keeper.getRawMaterialGoods();
        System.out.println(rawMaterialGoods.goods);
        
        //GET FINISHED GOODS
        FinishedGoods finishedGoods = keeper.getFinishedGoods();
        System.out.println(finishedGoods.goods);
        
        //GET PACKING MATERIAL GOODS
        PackingMaterialGoods packingMaterialGoods = keeper.getPackingMaterialGoods();
        System.out.println(packingMaterialGoods.goods);
        
        //ALTERNATE VERSION
        
        Goods rawMaterial = new StoreKeeper().getGoods("Raw");
        System.out.println(rawMaterial.toString());
        
        Goods packingMaterial = new StoreKeeper().getGoods("Packaging");
        System.out.println(packingMaterial.toString());
        
        Goods finished = new StoreKeeper().getGoods("Finished");
        System.out.println(finished.toString());
        

    }
    
}// End of class
    
