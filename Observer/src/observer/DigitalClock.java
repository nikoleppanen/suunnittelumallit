/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Niko
 */
public class DigitalClock implements Observer {
    

    @Override
    public void update(Observable o, Object arg) {
        //Observe
        ClockTimer ct = (ClockTimer)o;
        System.out.println(+ct.getHour()+":"+ct.getMin()+":"+ct.getSec());
    }
    
}
