/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Niko
 */
public class ClockTimer extends Observable implements Runnable{
    private int hour;
    private int minute;
    private int second;
            
        
    public int getHour(){
        return hour;
    }
    public int getMin(){
        return minute;
    }
    public int getSec(){
        return second;
    }
    // Runnable Thread
    public void tick(){
        second++;
        if(second>59){
            second = 0;
            minute++;
        }
        if(minute>59){
            minute = 0;
            hour++;
        }
        if(hour>23){
            hour = 0;
        }
    }

    @Override
    public void run() {
        while(true){
            tick();
            setChanged();
            notifyObservers(this.toString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClockTimer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
}
