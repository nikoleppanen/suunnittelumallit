/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Niko
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List list = new ArrayList();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");
        list.add("F");
        list.add("G");
        
        System.out.println("TWO ITERATORS ITERATING SAME ARRAYLIST");
        IteratorThread itr1 = new IteratorThread();
        IteratorThread itr2 = new IteratorThread();
        itr1.setList(list);
        itr2.setList(list);
        new Thread(itr1).start();
        new Thread(itr2).start();
        
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        
        System.out.println("TWO THREADS SHARING AN ITERATOR");
        Iterator shared_itr = null;
        itr1.setIterator(shared_itr);
        itr2.setIterator(shared_itr);
        itr1.setList(list);
        itr2.setList(list);
        new Thread(itr1).start();
        new Thread(itr2).start();
    }
    
}
