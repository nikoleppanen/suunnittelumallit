/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.*;

/**
 *
 * @author Niko
 */
public class IteratorThread implements Runnable {
    
    List list;
    Iterator itr;
    
    public IteratorThread(){
        
    }
    
    @Override
    public void run() {
        
        while(this.itr.hasNext()){
            Object obj = this.itr.next();
            System.out.println(Thread.currentThread().getName() +" : "+ obj + " : " + this.itr);
            try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        }
        
        System.out.println(Thread.currentThread().getName() +" exiting.");
    }
    
    public void setList(List list){
        this.list = list;
        this.itr = this.list.iterator();
    }
    
    public void setIterator(Iterator itr){
        this.itr = itr;
        this.itr = this.list.iterator();
    }
    
}
